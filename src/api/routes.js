import axios from "axios";


export const LOGIN_ROUTE = 'http://localhost:3001/api/v1/user/login';
export const PROFILE_ROUTE = 'http://localhost:3001/api/v1/user/profile';

/**
 * 
 * @param {String} endPoint Api route
 * @param {String} token JWT
 * @returns romise
 */
export const apiProfile = (endPoint, token) => {

    return axios.post(endPoint,
        {

        },
        {
            'headers': {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            }
        }
    )
}

/**
 *  function Api allow to edit a profile
 * @param {String} endPoint  Api route
 * @param {string} firstNameParam input FirstName value
 * @param {string} lastNameParam input lasttName value
 * @param {String} token JWT
 * @returns promise
 */
export const apiEdit = (endPoint, firstNameParam, lastNameParam, token) => {

    return axios.put(endPoint,
        {
            'firstName': firstNameParam,
            'lastName': lastNameParam
        },
        {
            'headers': {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            }
        }
    )
}

