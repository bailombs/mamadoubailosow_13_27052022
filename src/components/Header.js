import React from 'react';
import { AiOutlineExport } from 'react-icons/ai';
import { FaUserCircle } from 'react-icons/fa';
import { useDispatch, useSelector } from 'react-redux';
import { NavLink } from 'react-router-dom';
import logo from '../assets/img/argentBankLogo.png';
import { logOut } from '../features/slicer';


const Header = ({ page }) => {

    const firstName = useSelector((state) => state.connect.firstName)
    const dispatch = useDispatch()
    const pageValue = page === 'profile'

    return (
        <nav className='header'>
            <div className='header__logo'
                style={{ flex: `${pageValue}` ? '1' : '' }}
                onClick={() => dispatch(logOut())}
            >
                <NavLink to="/" className='header__logo--item'>
                    <img src={logo} alt="Argent Bank logo" />
                </NavLink>
            </div>

            {
                pageValue ? (
                    <React.Fragment>
                        <div className='header__sign' style={{ marginRight: '1rem' }}>
                            <NavLink to="/profile" className='header__sign--item'>
                                <FaUserCircle /> <span>{firstName}</span>
                            </NavLink>
                        </div>
                        <div className='header__out' onClick={() => {
                            dispatch(logOut())
                            sessionStorage.removeItem('token')
                        }}>

                            <NavLink to="/sign-in" className='header__sign--item'>
                                <AiOutlineExport /> <span>Sign Out</span>
                            </NavLink>
                        </div>
                    </React.Fragment>
                ) : (

                    <div className='header__sign'>
                        <NavLink to="/sign-in" className='header__sign--item'>
                            <FaUserCircle /> <span>Sign In</span>
                        </NavLink>
                    </div>
                )
            }
        </nav>
    );
};

export default Header;