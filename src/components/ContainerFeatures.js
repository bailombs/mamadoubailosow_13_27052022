import React from 'react';
import iconChat from '../assets/img/icon-chat.png';
import iconMoney from '../assets/img/icon-money.png';
import iconSecurity from '../assets/img/icon-security.png';
import Feature from './Feature';

const ContainerFeatures = () => {

    const features = [
        {
            icon: iconChat,
            title: 'You are #1 priority',
            text: `Need to talk to a representative ?you can get in touch through
             our 24/7 chat or through a phone call in less than 5 minutes
            `
        },
        {
            icon: iconMoney,
            title: 'You are #1 priority',
            text: `The more you save with us, the higher your interest rate will be!`
        },
        {
            icon: iconSecurity,
            title: 'You are #1 priority',
            text: `We use top of the line encryption to make sure your data and money is always safe.`

        }
    ]

    return (
        <section className='features'>
            {
                features.map((feature) =>
                    <Feature key={feature.icon}
                        icon={feature.icon}
                        title={feature.title}
                        text={feature.text}
                    />
                )
            }
        </section>
    );
};

export default ContainerFeatures;