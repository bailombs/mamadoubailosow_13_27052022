# ARGENT BANK

A React web application for the new user authentication system, Create, authenticate successfully, update the full name.

## Prerequisites

- [Node.js (v16.13.1)](https://nodejs.org/en/)
- [Recommended IDE (Visual Studio Code)](https://code.visualstudio.com)

## Installation

First of all, clone the project with HTTPS

```bash
  git clone https://gitlab.com/bailombs/mamadoubailosow_13_27052022.git
```

The second time you need to install dependencies for the Back_end and the Front_end

### Back-end

Need to be inside the root of the back_end folder, and run these commands (install dependencies, and run locally).

> Always start by run the back_end_sport_see before the front_end_sport_see forlder

```
cd back_end
yarn install or npm install
yarn dev:server
```

### Front-end

If you are on the back_end folder, make sure to come back to the root folder of the project with cd ..
So

```
yarn install or npm install
yarn start
```

## Generate documentation swagger

In the back_end folder you can export the file transation_swagger on the editor swagger

## Dependencies

| Name             | Version |
| ---------------- | ------- |
| react            | 18.1.0  |
| react-dom        | 18.1.0  |
| react-scripts    | 5.0.1   |
| react-icons      | 4.3.1   |
| react-redux      | 2.1.8   |
| redux toolkit    | 8.0.2   |
| jsdoc            | 3.6.10  |
| axios            | 0.27.2  |
| react-router-dom | 6.3.0   |
